(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
(setq neo-smart-open t)
;; 隠しファイルをデフォルトで表示
(setq neo-show-hidden-files t)

;; neotree でファイルを新規作成した後、自動的にファイルを開く
(setq neo-create-file-auto-open t)
