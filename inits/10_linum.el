;; 左側に行番号を表示
(require 'linum)
(global-linum-mode t)
(setq linum-format "%5d ")
