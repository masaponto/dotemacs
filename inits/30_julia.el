(autoload 'julia-mode "julia-mode" "Emacs mode for Julia" t)
(add-to-list 'auto-mode-alist '("\\.jl\\'" . julia-mode))
