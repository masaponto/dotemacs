(global-anzu-mode 1)

(custom-set-variables
 '(anzu-mode-lighter "")
 '(anzu-deactivate-region t)
 '(anzu-search-threshold 1000))

(global-set-key (kbd "M-%") 'anzu-query-replace)
(global-set-key (kbd "M-#") 'anzu-query-replace-at-cursor-thing)
